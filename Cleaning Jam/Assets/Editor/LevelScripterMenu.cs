﻿using UnityEditor;
using UnityEngine;
using System.Collections.Generic;

public class MenuTest : MonoBehaviour
{
    // Add a menu item named "Do Something" to MyMenu in the menu bar.
    [MenuItem("Tanner/Test")]
    static void DoSomething()
    {
        Debug.Log("Edits worked!");
    }

    [MenuItem("Derrick/Save Level As")]
    static void LogFile()
    {
        List<GameObject> objects = new List<GameObject>();
        List<string> strings = new List<string>();

        objects.AddRange(GameObject.FindGameObjectsWithTag("Enemy"));

        string tempName = System.IO.Path.GetFileNameWithoutExtension(EditorApplication.currentScene);

        var path = EditorUtility.SaveFilePanelInProject("Save texture as PNG",
                                 tempName + ".txt",
                                "txt",
                                "Please enter a file name to save the texture to");

        if (path != null)
        {
            foreach (GameObject o in objects)
            {
                strings.Add(o.name + " " + o.transform.position);
                System.IO.File.AppendAllText(path, o.name + "," + o.transform.position.x + "," + o.transform.position.y + ";");
            }
        }
        else
        {
            EditorUtility.DisplayDialog("Oops!", "Path didn't parse. Nothing saved.", "OK");
        }
    }
}