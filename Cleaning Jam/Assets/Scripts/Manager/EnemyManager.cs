﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyManager : MonoBehaviour 
{
    public TextAsset levelScript;

    List<EnemySpawn> spawns;

    bool deployedEnemies = false;

    public bool HasDeployedEnemies
    {
        get
        {
            return deployedEnemies;
        }
    }

	// Use this for initialization
	void Start ()
    {

	}

    public void SpawnEnemies()
    {
        spawns = new List<EnemySpawn>();

        List<string> splitStrings = new List<string>();

        splitStrings.AddRange(levelScript.text.ToString().Split(';'));

        //Debug.Log(splitStrings.Count);

        foreach (string s in splitStrings)
        {
            if (s.Length > 1)
            {

                //Debug.Log(s);
                EnemySpawn enemy = new EnemySpawn();

                string[] parameters = s.Split(',');

                //foreach (string st in parameters)
                //    Debug.Log(st);

                enemy.name = parameters[0];
                enemy.x = float.Parse(parameters[1]);
                enemy.y = float.Parse(parameters[2]);

                spawns.Add(enemy);
            }
        }

        //Debug.Log(spawns.Count);

        foreach (EnemySpawn e in spawns)
        {
            GameObject enemy = (GameObject)Instantiate(Resources.Load(e.name));
            enemy.transform.position = new Vector3(e.x, e.y, enemy.transform.position.z);
        }

        deployedEnemies = true;
    }

	// Update is called once per frame
	void Update () 
    {
	}

    struct EnemySpawn
    {
        public string name;
        public float x;
        public float y;
    }
}
