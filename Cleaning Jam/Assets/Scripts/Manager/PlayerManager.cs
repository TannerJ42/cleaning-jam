﻿using UnityEngine;
using System.Collections;

public class PlayerManager : MonoBehaviour 
{
    public GameObject playerPrefab;
    public int startingLives;

    

    GameObject player;
    int currentLives;

	// Use this for initialization
	void Start () 
    {
        currentLives = startingLives;

        SpawnPlayer();
	}
	
	// Update is called once per frame
	void Update () 
    {


        if (player == null)
        {
            if (currentLives > 0)
            {
                currentLives -= 1;
                SpawnPlayer();
            }
        }
	}

    public void SpawnPlayer()
    {
        Debug.Log("Spawning player.");
        player = (GameObject)Instantiate(playerPrefab);
    }
}
