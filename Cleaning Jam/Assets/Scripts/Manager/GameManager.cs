﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour 
{
    public int firstLevel = 0;

    public GameObject EnemyManager;

    EnemyManager manager;

    int currentLevel;

    float timer;

    float timeBetweenScans = 1f;

	// Use this for initialization
	void Start () 
    {
        currentLevel = firstLevel;

        GameObject managerObject = (GameObject)Instantiate(EnemyManager, transform.position, transform.rotation);

        manager = managerObject.GetComponent<EnemyManager>();
	}
	
	// Update is called once per frame
	void Update () 
    {
        //Debug.Log(manager == null);

        // check to see if there is no enemy manager. create one if there is and load the next level.
        //if (manager == null)
        //{
        //    ReloadManager();

        //    if (manager == null)
        //    {
        //        Debug.Log("Couldn't load 'EnemyManager'");
        //    }
        //}

        if (timer <= 0)
            Scan();
        else
            timer -= Time.deltaTime;
	}

    private void Scan()
    {
        timer = timeBetweenScans;

        if (!EnemiesLeft)
        {
            AdvanceLevel();
        }
    }

    private TextAsset LoadNextLevelScript()
    {
        string assetName = "";

        if (currentLevel < 10)
        {
            assetName = "00" + currentLevel.ToString();
        }
        else if (currentLevel < 100)
        {
            assetName = "0" + currentLevel.ToString();
        }
        else
            assetName = currentLevel.ToString();

        Debug.Log("Loading level " + assetName);

        currentLevel += 1;        

        return Resources.Load<TextAsset>("Levels/" + assetName);
    }

    public void AdvanceLevel()
    {
        //Destroy(manager, secondsBetweenWaves);
        //Debug.Log("Advancing level!");

        manager.levelScript = LoadNextLevelScript();

        Debug.Log(manager.levelScript.name);

        manager.SpawnEnemies();
    }

    public bool EnemiesLeft
    {
        get 
        {
            if (!manager.HasDeployedEnemies)
                return false;
            return (GameObject.FindGameObjectsWithTag("Enemy").Length > 0);
        }
    }
}
