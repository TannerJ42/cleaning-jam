﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StarManager : MonoBehaviour 
{
    List<Star> stars;
    public Texture2D[] textures;
    public float speedFactor = 1f;
    public int frequency = 1;

    Rect screenArea;

	// Use this for initialization
	void Start () 
    {
        stars = new List<Star>();

        screenArea = new Rect(0, 0, camera.pixelWidth, camera.pixelHeight);

        CreateInitialStars();

        frequency = 100 - frequency;
	}
	
	// Update is called once per frame
	void Update () 
    {

	}

    void FixedUpdate()
    {
        DoUpdate(Time.deltaTime);
    }

    void DoUpdate(float time)
    {
        for (int i = stars.Count - 1; i >= 0; i--)
        {
            stars[i].Update(time);
            if (stars[i].currentY > screenArea.height)
                stars.RemoveAt(i);
        }

        if (Random.Range(0, frequency) == 1)
            GenerateStar(Random.Range(1, 5), 0);
    }

    void OnGUI()
    {
        foreach (Star s in stars)
            s.Draw();
    }

    void CreateInitialStars()
    {
        for (int i = 0; i <= 1000; i++)
        {
            for (int x = 0; x <= 30; x++)
                DoUpdate(1f / 30f);
        }
    }

    void GenerateStar(float maxX)
    {
        Vector2 location = new Vector2(Random.Range(0, maxX), Random.Range(-10f, 0));

        stars.Add(new Star(textures[Random.Range(0, textures.Length - 1)], location, Random.Range(0.1f, 0.7f), Random.Range(10f * speedFactor, 25f * speedFactor)));
    }

    void GenerateStar(int number, float maxX)
    {
        for (int i = 0; i < number; i++)
            GenerateStar(screenArea.width);
    }


    class Star
    {
        float speed;
        Texture2D texture;
        Rect rect;

        public float currentY
        {
            get
            {
                return rect.y;
            }
        }

        public Star(Texture2D texture, Vector2 center, float scale, float speed)
        {
            this.texture = texture;
            rect = new Rect(center.x, center.y, texture.width * scale, texture.height * scale);
            this.speed = speed;
        }

        public void Draw()
        {
            //Graphics.DrawTexture(rect, texture);
            GUI.DrawTexture(rect, texture);
        }

        public void Update(float time)
        {
            rect.y += speed * time;
        }
    }
}
