﻿using UnityEngine;
using System.Collections;

public class LoadSceneOnClick : MonoBehaviour 
{
    public string SceneName = "Gameplay";

	// Use this for initialization
	void Start () 
    {
        
	}

	// Update is called once per frame
	void Update () 
    {
	    
	}

    void OnClick()
    {
        Application.LoadLevel(SceneName);
    }
}
