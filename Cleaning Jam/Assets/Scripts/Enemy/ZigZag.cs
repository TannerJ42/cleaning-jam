﻿using UnityEngine;
using System.Collections;

public class ZigZag : MonoBehaviour 
{
    public float timeBetweenZigs = 1f;
    float angle = 45f;
    public Direction direction = Direction.Left;

    float currentAngle;
    Transform myTransform;
    EnemyController controller;
    float timer;

	// Use this for initialization
	void Start () 
    {
        controller = gameObject.GetComponent<EnemyController>();
        myTransform = gameObject.GetComponent<Transform>();

        Zig();
        angle *= 2;
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (timer <= 0)
            Zig();
        else
            timer -= Time.deltaTime;
	}

    void Zig()
    {
        timer = timeBetweenZigs;

        if (direction == Direction.Left)
        {
            direction = Direction.Right;
            currentAngle = -angle;
            controller.direction = new Vector2(-1, -1);
        }
        else
        {
            direction = Direction.Left;
            currentAngle = angle;
            controller.direction = new Vector2(1, -1);
        }

        controller.direction.Normalize();

        myTransform.Rotate(new Vector3(0, 0, currentAngle));
    }

    public enum Direction
    {
        Left,
        Right,
        Up,
        Down
    }
}
