﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShootProjectiles : MonoBehaviour
{
    float timer;
    public float timeBetweenShots = 1f;
    public GameObject projectilePrefab;
    public int projectileCount = 1;

    public ShootDirection shootDirection = ShootDirection.Down;

    List<GameObject> projectiles;

    GameObject player;

    bool AllProjectilesAreInUse
    {
        get
        {
            if (projectiles.Count == 0)
                return true;
            foreach (GameObject g in projectiles)
            {
                if (!g.activeSelf)
                    return false;
            }

            return true;
        }
    }

    // Use this for initialization
    void Start()
    {
        timer = timeBetweenShots;

        projectiles = new List<GameObject>();

        for (int i = 0; i < projectileCount; i++)
        {
            GameObject projectile = (GameObject)Instantiate(projectilePrefab);
            projectile.SetActive(false);
            projectiles.Add(projectile);
        }

        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        if (timer <= 0)
            TryToShoot();
        else
        {
            timer -= Time.deltaTime;
        }

    }

    void TryToShoot()
    {

        foreach (GameObject g in projectiles)
        {
            if (!g.activeInHierarchy)
            {
                Shoot(g);
                return;
            }
        }

        return;
    }

    void Shoot(GameObject projectile)
    {
        projectile.transform.position = transform.position;

        GenericProjectile projController = projectile.GetComponent<GenericProjectile>();

        if (shootDirection == ShootDirection.AtPlayer)
        {
            if (player != null)
            {
                Vector2 direction = player.transform.position - transform.position;
                direction.Normalize();
                projController.direction = direction;
            }
        }

        projectile.SetActive(true);
        timer = timeBetweenShots;
    }

    public enum ShootDirection
    {
        Down,
        AtPlayer
    }
}
