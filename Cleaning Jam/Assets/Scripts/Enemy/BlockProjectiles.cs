﻿using UnityEngine;
using System.Collections;

public class BlockProjectiles : MonoBehaviour 
{
    public bool DestroyFriendlyLasers = true;
    public bool DestroyEnemyLasers = true;

	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    void OnTriggerEnter2D(Collider2D c)
    {
        if (c.tag != "PlayerLaser" &&
            c.tag != "EnemyLaser")
        {
            return;
        }

        if (DestroyFriendlyLasers &&
            c.tag == "PlayerLaser")
        {
            c.gameObject.SetActive(false);
        }
        if (DestroyEnemyLasers &&
            c.tag == "EnemyLaser")
        {
            c.gameObject.SetActive(false);
        }
    }
}
