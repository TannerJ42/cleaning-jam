﻿using UnityEngine;
using System.Collections;

public class ShieldManager : MonoBehaviour 
{
    public GameObject[] shields;

    public bool[] activeShields;

    public float timeBetweenRotations;

    public RotationDirection rotationDirection = RotationDirection.Clockwise;

    float timer;

	// Use this for initialization
	void Start () 
	{
        timer = timeBetweenRotations;

        for (int i = shields.Length - 1; i >= 0; i--)
        {
            if (activeShields[i] == true)
                shields[i].SetActive(true);
            else
                shields[i].SetActive(false);
        }
	}

    // Update is called once per frame
    void Update() 
	{
        if (timer <= 0)
            Rotate();
        else
            timer -= Time.deltaTime;
	}

    private void Rotate()
    {
        timer = timeBetweenRotations;

        bool[] newActiveShields = new bool[activeShields.Length];

        for (int i = 0; i <= newActiveShields.Length - 1; i++)
        {
            newActiveShields[i] = false;
        }

        for (int i = 0; i <= activeShields.Length - 1; i++)
        {
            bool setting = false;

            if (activeShields[i] == true)
            {
                //if (rotationDirection == RotationDirection.Clockwise)
                //{
                //    setting = true;
                //}
                setting = true;
                Debug.Log((i + 1) % (shields.Length - 1));
            }

            int nextShieldNumber = i;

            if (rotationDirection == RotationDirection.Clockwise)
            {
                nextShieldNumber++;
                if (nextShieldNumber > newActiveShields.Length - 1)
                    nextShieldNumber = 0;
            }
            else
            {
                nextShieldNumber--;
                if (nextShieldNumber < 0)
                    nextShieldNumber = newActiveShields.Length - 1;
            }

            newActiveShields[nextShieldNumber] = setting;
        }

        // activate or deactivate shields according to newActiveShields
        for (int i = 0; i <= shields.Length - 1; i++)
        {
            if (newActiveShields[i] == true)
            {
                shields[i].SetActive(true);
            }
            else
            {
                shields[i].SetActive(false);
            }
        }

        activeShields = newActiveShields;
    }

    void FixedUpdate()
    {

    }

    public enum RotationDirection
    {
        Clockwise,
        Widdershins
    }
}
