﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour 
{
    public Vector2 direction = -Vector2.up;
    public Vector2 rotation = Vector2.zero;
    public float speed = 1;
    public int health = 100;
    public int collisionDamage = 100;

    public int score = 10;

    int currentHealth;
    Transform myTransform;

	// Use this for initialization
	void Start () 
    {
        currentHealth = health;
        myTransform = GetComponent<Transform>();
	}

    // FixedUpdate is called once per physics tick
    void FixedUpdate()
    {
        // not using += because of ambiguity between Vector2 and Vector3
        myTransform.position = (Vector2)myTransform.position + direction * speed * Time.deltaTime;
    }

	// Update is called once per frame
	void Update () 
    {
        if (currentHealth <= 0)
        {
            Die();
        }
	}

    void Die()
    {
        // TODO: more stuff

        ScoreManager.score += score;

        Destroy(this.gameObject);
    }

    void TakeDamage(int damage)
    {
        currentHealth -= damage;
    }

    void OnTriggerEnter2D(Collider2D c)
    {
        if (c.tag != "Player" &&
            c.tag != "Border")
            return;

        if (c.tag == "Player")
        {
            c.SendMessage("TakeDamage", collisionDamage);
            Die();
        }

        if (c.tag == "Border")
        {
            Destroy(this.gameObject);
        }
    }
}
