﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
    public float speed = 5;
    public int health = 100;

    float yAnchor;
    int currentHealth;
    Transform myTransform;

    public GameObject leftDrone;
    public GameObject rightDrone;

    bool immune = true;
    public float timeImmuneAfterSpawn = 1f;
    public float immuneFlashTimer = 0.07f;

    float flashTimer;

    // Use this for initialization
    void Start()
    {
        myTransform = gameObject.GetComponent<Transform>();

        yAnchor = myTransform.position.y;

        currentHealth = health;

        leftDrone = (GameObject)Instantiate(leftDrone);
        rightDrone = (GameObject)Instantiate(rightDrone);

        flashTimer = immuneFlashTimer;
    }

    // FixedUpdate is called once per physics tick
    void FixedUpdate()
    {
        float horizontalMovement = Input.GetAxis("Horizontal");

        float distanceThisTick = horizontalMovement * speed * Time.deltaTime;

        myTransform.position = new Vector3(myTransform.position.x + distanceThisTick, yAnchor, myTransform.position.z);
    }

    // Update is called once per frame
    void Update()
    {
        if (immune)
        {
            if (timeImmuneAfterSpawn <= 0)
            {
                immune = false;
                BoxCollider2D collider = GetComponent<BoxCollider2D>();
                collider.enabled = true;
                renderer.enabled = true;
            }
            else
            {
                if (flashTimer <= 0)
                {
                    flashTimer = immuneFlashTimer;
                    if (renderer.enabled)
                        renderer.enabled = false;
                    else
                        renderer.enabled = true;
                }
                else
                {
                    flashTimer -= Time.deltaTime;
                }

                timeImmuneAfterSpawn -= Time.deltaTime;
            }
        }

        if (currentHealth <= 0)
        {
            Die();
        }
    }

    void Die()
    {
        // TODO: Expand
        Destroy(this.gameObject);
        leftDrone.SendMessage("Die");
        rightDrone.SendMessage("Die");
    }

    void TakeDamage(int damage)
    {
        currentHealth -= damage;
    }
}
