﻿using UnityEngine;
using System.Collections;

public class DroneController : MonoBehaviour
{
    public float speed = 5;
    //public GameObject player;

    float xAnchor;
    Transform myTransform;

    // Use this for initialization
    void Start()
    {
        myTransform = gameObject.GetComponent<Transform>();

        xAnchor = myTransform.position.x;
    }

    // FixedUpdate is called once per physics tick
    void FixedUpdate()
    {
        float verticalMovement = Input.GetAxis("Vertical");

        float distanceThisTick = verticalMovement * speed * Time.deltaTime;

        myTransform.position = new Vector3(xAnchor, myTransform.position.y + distanceThisTick, myTransform.position.z);
    }

    // Update is called once per frame
    void Update()
    {
        //if (player == null)
        //{
        //    Debug.Log("No player. Dying!");
        //    Die();
        //}
    }

    void Die()
    {
        // TODO: Expand
        Destroy(this.gameObject);
    }
}
