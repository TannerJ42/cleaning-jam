﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerShootProjectile : MonoBehaviour
{
    float timer;
    public float timeBetweenShots = 1f;
    public GameObject projectilePrefab;
    public int projectileCount = 1;

    List<GameObject> projectiles;

    bool WantsToShoot
    {

        get
        {
            return Input.GetAxis("Fire1") > 0;
        }
    }

    bool AllProjectilesAreInUse
    {
        get
        {
            if (projectiles.Count == 0)
                return true;
            foreach (GameObject g in projectiles)
            {
                if (!g.activeSelf)
                    return false;
            }

            return true;
        }
    }

    // Use this for initialization
    void Start()
    {
        timer = timeBetweenShots;

        projectiles = new List<GameObject>();

        for (int i = 0; i < projectileCount; i++)
        {
            GameObject projectile = (GameObject)Instantiate(projectilePrefab);
            projectile.SetActive(false);
            projectiles.Add(projectile);

            GenericProjectile s = projectile.GetComponent<GenericProjectile>();
            s.targetTag = "Enemy";
            s.direction = Vector2.up;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (timer <= 0 &&
            WantsToShoot)
            TryToShoot();
        else
        {
            timer -= Time.deltaTime;
        }

    }

    void TryToShoot()
    {
        foreach (GameObject g in projectiles)
        {
            if (!g.activeInHierarchy)
            {
                Shoot(g);
                return;
            }
        }

        return;
    }

    void Shoot(GameObject projectile)
    {
        projectile.transform.position = transform.position;

        projectile.SetActive(true);

        projectile.SendMessage("PlaySound");

        timer = timeBetweenShots;
    }
}
