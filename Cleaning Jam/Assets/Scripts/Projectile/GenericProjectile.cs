﻿using UnityEngine;
using System.Collections;

public class GenericProjectile : MonoBehaviour 
{
    public Vector2 direction = -Vector2.up;
    public float speed = 7;
    public int damage = 100;
    public string targetTag = "Player";

    Transform myTransform;

    public AudioClip[] sounds;
    public float minimumPitch = 0.5f;
    public float maximumPitch = 1.5f;

	// Use this for initialization
	void Start () 
    {
        myTransform = gameObject.GetComponent<Transform>();
	}

    // FixedUpdate is called once per physics tick
    void FixedUpdate()
    {
        // not using += because of ambiguity between Vector2 and Vector3
        myTransform.position = (Vector2)myTransform.position + direction * speed * Time.deltaTime;
    }

	// Update is called once per frame
	void Update () 
    {
	    
	}

    void OnTriggerEnter2D(Collider2D c)
    {
        if (c.tag != targetTag &&
            c.tag != "Border" &&
            c.tag != "ProjectileWall")
            return;
        

        if (c.tag == targetTag)
        {
            c.SendMessage("TakeDamage", damage);
        }
        
        gameObject.SetActive(false);
    }

    public void PlaySound()
    {
        if (sounds.Length > 0)
        {
            AudioSource source = GetComponent<AudioSource>();

            source.pitch = Random.Range(minimumPitch, maximumPitch);

            source.clip = sounds[Random.Range(0, sounds.Length - 1)];

            source.Play();
        }
    }
}
